# NestjsTasks
This project is a REST api with pretty extensive protection (with jwt's).
You can start it just as any project: `yarn start:dev` or `yarn start:debug` (or with npm: `npm run start:debug`).

## About the testing
Take notice that not everything is tested, and this is intentional (so please don't check for coverage ;) ). The aim of this project was getting to know the framework and make sure that testing is possible and works well. When I found that it was no different from the other languages and frameworks I know (especially Angular), I was sure that I could successfully create tests for most use-cases already.
Saves time for learning more stuff! 

Best regards,
Aart

import { Body, Controller, Post, ValidationPipe } from '@nestjs/common';

import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) { }

    @Post('/sign-up')
    async signUp(
        @Body(ValidationPipe)
        authCredentialsDto: AuthCredentialsDto
    ) {
        return this.authService.signUp(authCredentialsDto);
    }

    @Post('/sign-in')
    async signIn(
        @Body(ValidationPipe)
        authCredentialsDto: AuthCredentialsDto
    ) {
        return this.authService.signIn(authCredentialsDto);
    }
}

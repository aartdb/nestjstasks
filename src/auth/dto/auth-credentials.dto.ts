import { IsString, MaxLength, MinLength } from 'class-validator';

export class AuthCredentialsDto {
    @IsString()
    @MinLength(2)
    @MaxLength(50)
    username: string;

    @IsString()
    @MinLength(4, { message: 'Take at least some care that it will be safe. It should be at least 4 chars long.' })
    @MaxLength(255)
    password: string;
}
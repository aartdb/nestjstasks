import { createParamDecorator, ExecutionContext } from '@nestjs/common';

import { User } from './user.entity';

/**
 * Get the user from the Bearer Authorization
 */
export const GetUser = createParamDecorator((data, ctx: ExecutionContext): User => {
    const req = ctx.switchToHttp().getRequest();
    const user: User = req.user;
    return user;
});
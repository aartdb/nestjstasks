import { UnauthorizedException } from '@nestjs/common';
import { Test } from '@nestjs/testing';

import { JwtPayload } from './jwt-payload.interface';
import { JwtStrategy } from './jwt.strategy';
import { User } from './user.entity';
import { UserRepository } from './user.repository';

class MockUserRepository {
    findOne = jest.fn();
}
describe(`JwtStrategy`, () => {
    let jwtStrategy: JwtStrategy;
    let userRepository: MockUserRepository;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [
                JwtStrategy,
                { provide: UserRepository, useClass: MockUserRepository },
            ],
        }).compile();

        userRepository = module.get(UserRepository);
        jwtStrategy = module.get(JwtStrategy);
    });

    describe(`validate`, () => {
        let user: User;
        let payload: JwtPayload;

        beforeEach(() => {
            user = new User();
            user.username = `username ${Math.random()}`;
            payload = { username: user.username };
            userRepository.findOne.mockResolvedValue(user);
        });

        const exec = () => jwtStrategy.validate(payload);

        it(`should return the user if valid`, async () => {
            const result = await exec();

            expect(result).toEqual(user);
        });
        it(`should throw UnauthorizedException if no user is found`, async () => {
            userRepository.findOne.mockResolvedValue(null);

            await expect(exec()).rejects.toThrow(UnauthorizedException);
        });
    });

});
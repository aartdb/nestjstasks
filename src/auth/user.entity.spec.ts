import * as bcrypt from 'bcryptjs';

import { User } from './user.entity';

describe(`User`, () => {
    describe(`validatePassword`, () => {
        let passwordToReturn: string;
        let user: User;
        bcrypt.hash = jest.fn();

        beforeEach(() => {
            passwordToReturn = `password ${Math.random()}`;
            bcrypt.hash.mockReturnValue(passwordToReturn);

            user = new User();
            user.salt = '1234';
            user.password = passwordToReturn;
        });

        const exec = () => user.validatePassword(passwordToReturn);

        it(`should use an encryption`, async () => {
            await exec();

            expect(bcrypt.hash).toHaveBeenCalledWith(passwordToReturn, user.salt);
        });

        it(`should return true if passwords are the same`, async () => {
            expect(await exec()).toBeTruthy();
        });

        it(`should return false if passwords are NOT the same`, async () => {
            user.password = 'different password';
            expect(await exec()).toBeFalsy();
        });
    });
});
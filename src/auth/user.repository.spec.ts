import * as bcrypt from 'bcryptjs';

import { ConflictException, InternalServerErrorException } from '@nestjs/common';
import { Test } from '@nestjs/testing';

import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { User } from './user.entity';
import { UserRepository } from './user.repository';

const mockCredentialsDto: AuthCredentialsDto = { username: '1234', password: '1234' };

describe(`UserRepository`, () => {
    let userRepository: UserRepository;

    beforeEach(async () => {
        const module = Test.createTestingModule({
            providers: [
                UserRepository,
            ],
        }).compile();

        userRepository = (await module).get<UserRepository>(UserRepository);
    });

    describe(`signUp`, () => {
        const user = new User();
        user.save = jest.fn();

        beforeEach(() => {
            userRepository.create = jest.fn().mockReturnValue(user);
        });

        const exec = () => userRepository.signUp(mockCredentialsDto);

        it(`successfully saves the user`, async () => {
            expect(user.save).not.toHaveBeenCalled();

            await exec();

            expect(user.save).toHaveBeenCalled();
        });

        it(`saves the user with a hashed password`, async () => {
            const hashedPassword = `hashed ${Math.random()}`;
            bcrypt.hash = jest.fn().mockResolvedValue(hashedPassword);

            await exec();

            expect(bcrypt.hash).toHaveBeenCalledWith(mockCredentialsDto.password, user.salt);
            expect(user.password).not.toEqual(mockCredentialsDto.password);
            expect(user.password).toEqual(hashedPassword);
        });

        it(`throws a ConflictException if the user already exists`, async () => {
            (user.save as jest.Mock).mockRejectedValue({ code: '23505' }); // this is the error code that stands for a conflict

            await expect(exec()).rejects.toThrow(ConflictException);
        });

        it(`throws an InternalServerError if another error occurs`, async () => {
            (user.save as jest.Mock).mockRejectedValue({ code: 'unhandled-code' });

            await expect(exec()).rejects.toThrow(InternalServerErrorException);
        });
    });

    describe(`validateUserPassword`, () => {
        let user: User;

        beforeEach(() => {
            user = new User();
            user.username = `username ${Math.random()}`;
            user.validatePassword = jest.fn().mockResolvedValue(true);
            userRepository.findOne = jest.fn().mockResolvedValue(user);
        });

        const exec = () => userRepository.validateUser(mockCredentialsDto);

        it(`should have validated the password`, async () => {
            await exec();

            expect(user.validatePassword).toHaveBeenCalledWith(mockCredentialsDto.password);
        });

        it(`returns username if validation is successful`, async () => {
            const result = await exec();

            expect(result).toEqual(user.username);
        });

        describe(`sad path`, () => {
            it(`returns null if user can't be found`, async () => {
                (userRepository.findOne as jest.Mock).mockResolvedValue(null);

                const result = await exec();

                expect(result).toBeNull();
            });
            it(`returns null if password is invalid`, async () => {
                (user.validatePassword as jest.Mock).mockResolvedValue(false);

                const result = await exec();

                expect(result).toBeNull();
            });
        });
    });

});
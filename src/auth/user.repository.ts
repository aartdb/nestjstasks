import * as bcrypt from 'bcryptjs';
import { EntityRepository, Repository } from 'typeorm';

import { ConflictException, InternalServerErrorException } from '@nestjs/common';

import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { User } from './user.entity';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
    async signUp(authCredentialsDto: AuthCredentialsDto): Promise<void> {
        const { username, password } = authCredentialsDto;

        const user = this.create();
        user.username = username;
        user.salt = await bcrypt.genSalt();
        user.password = await this.hashPassword(password, user.salt);

        try {
            await user.save();
        } catch (err) {
            // if username (unique key) already exists
            if (err.code === '23505') {
                throw new ConflictException(`Username already exists`);
            }
            // else, make sure to bubble up the error
            throw new InternalServerErrorException();
        }
    }

    async validateUser(authCredentialsDto: AuthCredentialsDto): Promise<string> {
        const { username, password } = authCredentialsDto;
        const user = await this.findOne({ username });

        if (user && (await user.validatePassword(password))) {
            return user.username;
        }
        return null;
    }

    private hashPassword(password: string, salt: string): Promise<string> {
        return bcrypt.hash(password, salt);
    }
}
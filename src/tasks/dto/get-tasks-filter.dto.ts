import { IsIn, IsNotEmpty, IsOptional } from 'class-validator';

import { possibleTaskStatuses, TaskStatusEnum } from '../taskStatusEnum';

export class GetTasksFilterDto {
    @IsOptional()
    @IsIn(possibleTaskStatuses)
    status: TaskStatusEnum;

    @IsOptional()
    @IsNotEmpty()
    search: string;
}

import { BadRequestException } from '@nestjs/common';
import { PipeTransform } from '@nestjs/common/interfaces';

import { possibleTaskStatuses } from '../taskStatusEnum';

export class TaskStatusValidationPipe implements PipeTransform {
    transform(value: any) {
        if (!this.isStatusValid(value)) {
            throw new BadRequestException(`'${value}' is an invalid status.`);
        }
        return value;

    }

    private isStatusValid(status) {
        return possibleTaskStatuses.includes(status);
    }
}
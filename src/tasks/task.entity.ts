import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { User } from '../auth/user.entity';
import { TaskStatusEnum } from './taskStatusEnum';

@Entity()
export class Task extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    status: TaskStatusEnum;

    @ManyToOne(type => User, user => user.tasks, { eager: false })
    user: User;

    // userId is created automatically by TypeORM
    @Column()
    userId: number;
}
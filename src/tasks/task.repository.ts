import { User } from 'src/auth/user.entity';
import { EntityRepository, Repository } from 'typeorm';

import { InternalServerErrorException, Logger } from '@nestjs/common';

import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { Task } from './task.entity';
import { TaskStatusEnum } from './taskStatusEnum';

@EntityRepository(Task)
export class TaskRepository extends Repository<Task> {
    private logger = new Logger('TaskRepository');

    async findTasks(
        filterDto: GetTasksFilterDto,
        user: User
    ): Promise<Task[]> {
        const { search, status } = filterDto;

        const query = this.createQueryBuilder('task');
        query.where('task.userId = :userId', { userId: user.id });

        if (status) {
            query.andWhere('task.status = :status', { status });
        }

        if (search) {
            query.andWhere('(task.title LIKE :search OR task.description LIKE :search)', { search: `%${search}%` });
        }

        try {
            const tasks = await query.getMany();
            return tasks;
        } catch (err) {
            this.logger.error(`Failed to get tasks for user "${user.username}" with filters: ${JSON.stringify(filterDto)}`, err.stack);
            throw new InternalServerErrorException();
        }

    }

    async createTask(
        createTaskDto: CreateTaskDto,
        user: User
    ): Promise<Task> {
        const { title, description } = createTaskDto;

        const task = new Task();
        task.title = title;
        task.description = description;
        task.status = TaskStatusEnum.OPEN;
        task.user = user;


        try {
            await task.save();
        } catch (err) {
            this.logger.error(`Failed to create task for user "${user.username}" with data: ${JSON.stringify(createTaskDto)}`, err.stack);
            throw new InternalServerErrorException();
        }

        // don't return the user to the client
        delete task.user;
        // delete task.userId;

        return task;
    }
}
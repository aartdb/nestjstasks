export enum TaskStatusEnum {
    OPEN = 'OPEN',
    IN_PROGRESS = 'IN_PROGRESS',
    DONE = 'DONE'
}

export const possibleTaskStatuses = Object.values(TaskStatusEnum);
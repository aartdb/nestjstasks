import { GetUser } from 'src/auth/get-user.decorator';
import { User } from 'src/auth/user.entity';

import {
    Body, Controller, Delete, Get, Logger, Param, ParseIntPipe, Patch, Post, Query, UseGuards,
    UsePipes, ValidationPipe
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TaskStatusValidationPipe } from './pipes/task-status-validation';
import { Task } from './task.entity';
import { TasksService } from './tasks.service';
import { TaskStatusEnum } from './taskStatusEnum';

@Controller('tasks')
@UseGuards(AuthGuard())
export class TasksController {
    private logger = new Logger('TasksController');

    constructor(private tasksService: TasksService) { }

    @Get()
    getTasks(
        @Query(ValidationPipe) filterDto: GetTasksFilterDto,
        @GetUser() user: User
    ) {
        this.logger.verbose(`User "${user.username}" retrieving all tasks with filters: ${JSON.stringify(filterDto)}`);
        return this.tasksService.find(filterDto, user);
    }

    @Get('/:id')
    getById(
        @Param('id', ParseIntPipe) id: number,
        @GetUser() user: User
    ) {
        return this.tasksService.get(id, user);
    }

    @Post()
    @UsePipes(ValidationPipe)
    create(
        @Body() createTaskDto: CreateTaskDto,
        @GetUser() user: User
    ): Promise<Task> {
        this.logger.verbose(`User "${user.username}" creating task with data: ${JSON.stringify(createTaskDto)}`);

        return this.tasksService.create(createTaskDto, user);
    }

    @Delete('/:id')
    delete(
        @Param('id', ParseIntPipe) id: number,
        @GetUser() user: User
    ) {
        return this.tasksService.delete(id, user);
    }

    @Patch('/:id/status')
    updateStatus(
        @Param('id', ParseIntPipe) id: number,
        @Body('newValue', TaskStatusValidationPipe) newStatus: TaskStatusEnum,
        @GetUser() user: User
    ) {
        return this.tasksService.updateStatus(id, newStatus, user);
    }
}

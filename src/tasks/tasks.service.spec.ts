import { NotFoundException } from '@nestjs/common';
import { Test } from '@nestjs/testing';

import { User } from '../auth/user.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { Task } from './task.entity';
import { TaskRepository } from './task.repository';
import { TasksService } from './tasks.service';
import { TaskStatusEnum } from './taskStatusEnum';

class MockTaskRepository {
  findTasks = jest.fn();
  findOne = jest.fn();
  createTask = jest.fn();
  delete = jest.fn();
  updateStatus = jest.fn();
}

const mockUser = new User();
mockUser.username = 'Aart';

describe('TasksService', () => {
  let tasksService: TasksService;
  let taskRepository: MockTaskRepository;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        TasksService,
        {
          provide: TaskRepository,
          useClass: MockTaskRepository,
        },
      ],
    }).compile();

    tasksService = module.get(TasksService);
    taskRepository = module.get(TaskRepository);
  });


  describe(`find`, () => {
    it(`should get all tasks from the repository`, async () => {
      const resolvedTasks = 'doesnt-matter';
      taskRepository.findTasks.mockResolvedValue(resolvedTasks);
      expect(taskRepository.findTasks).not.toHaveBeenCalled();

      const filterDto: GetTasksFilterDto = { status: TaskStatusEnum.IN_PROGRESS, search: 'Searchquery' };
      const result = await tasksService.find(filterDto, mockUser);

      expect(taskRepository.findTasks).toHaveBeenCalledWith(filterDto, mockUser);
      expect(result).toEqual(resolvedTasks);
    });
  });

  describe('get', () => {
    it(`should successfully retrieve and return the task`, async () => {
      const mockTask = new Task();
      mockTask.description = '12345';
      mockTask.title = '12345';
      // as tasksService redirects this request to taskRepository, mock that
      taskRepository.findOne.mockResolvedValue(mockTask);

      const irrelevantTaskId = 123;
      const result = await tasksService.get(irrelevantTaskId, mockUser);

      expect(result).toEqual(mockTask);
    });

    it(`should throw a NotFoundException if the task does not exist`, async () => {
      taskRepository.findOne.mockResolvedValue(null);

      const irrelevantTaskId = 123;
      const result = tasksService.get(irrelevantTaskId, mockUser);

      expect(result).rejects.toThrow(NotFoundException);
    });
  });

  describe(`create`, () => {
    it(`should redirect the creation of the task to the repo and return that`, async () => {
      const task = new Task();
      task.description = `new desc ${Math.random()}`;
      taskRepository.createTask.mockResolvedValue(task);
      expect(taskRepository.createTask).not.toHaveBeenCalled();

      const createDto: CreateTaskDto = { title: '12345', description: '12345' };
      const result = await tasksService.create(createDto, mockUser);

      expect(result).toEqual(task);
      expect(taskRepository.createTask).toHaveBeenCalled();
    });
  });


  describe(`delete`, () => {
    it(`should redirect the deletion to the repo`, async () => {
      const resolvedVal = { affected: 1 };
      taskRepository.delete.mockResolvedValue(resolvedVal);
      expect(taskRepository.delete).not.toHaveBeenCalled();

      await tasksService.delete(1, mockUser);

      expect(taskRepository.delete).toHaveBeenCalledWith({ id: 1, userId: mockUser.id });
    });

    it(`should throw a NotFoundException if nothing has been affected`, async () => {
      expect(taskRepository.delete).not.toHaveBeenCalled();
      taskRepository.delete.mockResolvedValue({ affected: 0 });

      await expect(tasksService.delete(1, mockUser)).rejects.toThrow(NotFoundException);
    });
  });

  describe(`updateStatus`, () => {
    it(`should get and update the task's status`, async () => {
      const task = new Task();
      task.id = 123;
      task.description = `unique ${Math.random()}`;
      task.status = TaskStatusEnum.OPEN;
      task.save = jest.fn();

      tasksService.get = jest.fn();
      (tasksService.get as jest.Mock).mockResolvedValue(task);

      const newStatus = TaskStatusEnum.IN_PROGRESS;
      const result = await tasksService.updateStatus(task.id, newStatus, mockUser);

      expect(task.save).toHaveBeenCalled();
      expect(result.status).toEqual(newStatus);
    });
  });


});
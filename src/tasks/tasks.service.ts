import { User } from 'src/auth/user.entity';

import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { Task } from './task.entity';
import { TaskRepository } from './task.repository';

@Injectable()
export class TasksService {
    constructor(
        @InjectRepository(TaskRepository)
        private taskRepository: TaskRepository
    ) { }

    find(
        filterDto: GetTasksFilterDto,
        user: User
    ) {
        return this.taskRepository.findTasks(filterDto, user);
    }

    async get(id: number, user: User): Promise<Task> {
        const found = await this.taskRepository.findOne({ where: { id, userId: user.id } });

        if (!found) {
            throw new NotFoundException(`Task with id '${id}' not found`);
        }

        return found;
    }

    async create(
        createTaskDto: CreateTaskDto,
        user: User
    ) {
        return this.taskRepository.createTask(createTaskDto, user);
    }

    async updateStatus(id: number, newStatus: any, user: User): Promise<Task> {
        const task = await this.get(id, user);

        task.status = newStatus;
        await task.save();

        return task;
    }

    async delete(id: number, user: User) {
        const res = await this.taskRepository.delete({ id, userId: user.id });

        if (!res.affected) {
            throw new NotFoundException(`Task with id '${id}' not found`);
        }
    }
}
